# Attempt 1

## Framing

### Terms
  - label: The thing we are predicting. It can be denoted as `y`
  - feature: The input data. It can be denoted as `x` and usualy, it is
    multi-dimensional vector.
  - labeled examples: The vector, `(x,y)` which is input data and its label
    combined.
  - un-labeled data: The vector, `(x, ?)` which is input data and unknown label
    combined.
  - model: The thing that defines relationship between features and label.
  - training: The process of creating/learning the model.
  - inference: The process of applying he trained model to unlabeled exaples.
  - regression: It is a model that predicts continuous valuess.
  - classification: It is a model that predicts discrete values.

## Descending into ML

### Terms

### Take aways
  - Training a model is finding good values for all the weights and bias.
    Meaning that finding good vlaues for `w,b` for `y = w*x + b`.
  - The empirical risk minimization is a process of examining many examples and
  attempt to find a model that minimizes error.
  - In order to minimize the error, we need a way to measure the error related
    to the label of a example.
  - Mean square error is a function that measure the error across all examples.
    The function is defined as `mse = 1/N * sum(y - prediction(x))^2`.

## Reducing Loss

### Terms
  - converged model is a model that stops or extremly changing of its loss over
    iterations.
  - learning rate is a scalar that is being multiplied by the gradient to
    determine the next position.

### Take aways
  - A gradient is a vector that has direction and magnitude and its direction
    points to where it would increase the most of value of the function.
  - Hessian can be used to find ideal learning rate of 2 or more dimensional
    vector.

## Generalization

### Terms

### Take aways
  - The loss of a model can be low but the model can be bad.
  - Overfitting can happen if by making the model complex than necessary.
  - The goal of ML is to predict well on new incoming data drawn from hidden
    probability distribution.
  - With a given set of example data, you can train and evaluate your model by
    dividing the data set into `training set` and `test set`. Good perfomanace
    on the test set indicates that your model works well if the test set is
    large enough and not using same test set many times.

## Representaion

### Terms
  - feature engineering means transformig raw data into a feature vector.
  - one-hot-encoding is way to represent data in a vector where the vector has
    only one 1 for corresponding category and 0 for others.



