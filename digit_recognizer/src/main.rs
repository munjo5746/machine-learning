// What do we need?
//  - Cost function to measure the error
//  - Derivative of the cost function for the correction
//  - Activation function

#[derive(Debug)]
struct Network {
    num_of_layers: usize,
    sizes: Vec<i32>, // [num_of_neurons_layer1, num_of_neurons_layer2, ...]
    biases: Vec<Vec<Vec<f64>>>,
    weights: Vec<Vec<Vec<f64>>>,
}

#[derive(Debug)]
struct NetworkBuilder {
    num_of_layers: usize,
    learning_rate: Option<f64>,
    training_data: Option<Vec<f64>>,
    sizes: Vec<i32>, // [num_of_neurons_layer1, num_of_neurons_layer2, ...]
    biases: Vec<Vec<Vec<f64>>>,
    weights: Vec<Vec<Vec<f64>>>,
}

impl NetworkBuilder {
    fn new(sizes: Vec<i32>) -> NetworkBuilder {
        // fill biases
        let mut biases: Vec<Vec<Vec<f64>>> = Vec::new();
        for i in 0..sizes.len() - 1 {
            let num_of_rows = sizes[i];

            let mut rows: Vec<Vec<f64>> = Vec::new();
            for j in 0..num_of_rows {
                let mut row: Vec<f64> = Vec::new();
                row.push(0f64);

                rows.push(row);
            }

            biases.push(rows);
        }

        // generate weights matrix for each layer
        let mut weights: Vec<Vec<Vec<f64>>> = Vec::new();
        for i in 0..sizes.len() {
            if i >= sizes.len() - 1 {
                break;
            }

            let num_of_rows = sizes[i];
            let num_of_cols = sizes[i + 1];

            let mut rows: Vec<Vec<f64>> = Vec::new();
            for _ in 0..num_of_rows {
                let mut row: Vec<f64> = Vec::new();
                for _ in 0..num_of_cols {
                    row.push(0f64);
                }
                rows.push(row);
            }

            weights.push(rows);
        }

        let networkBuilder = NetworkBuilder {
            num_of_layers: sizes.len(),
            learning_rate: None,
            training_data: None,
            sizes: sizes,
            biases: biases,
            weights: weights,
        };

        networkBuilder
    }

    fn set_training_data(&mut self, data: Vec<f64>) -> &NetworkBuilder {
        self.training_data = Some(data);

        self
    }
}

fn sigmoid(z: f64) -> f64 {
    1.0f64 / (1.0f64 + (-z).exp())
}

fn dot(v1: Vec<f64>, v2: Vec<f64>) -> f64 {
    if v1.len() != v2.len() {
        panic!("v1.len() != v2.len(), {} != {}", v1.len(), v2.len());
    }

    let mut sum = 0f64;
    for zipped in v1.iter().zip(v2.iter()) {
        let (val1, val2) = zipped;

        sum += val1 * val2;
    }

    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_dot() {
        let v1 = vec![1f64, 2f64, 3f64];
        let v2 = vec![3f64, 2f64, 1f64];

        assert_eq!(dot(v1, v2), 10f64);
    }

    #[test]
    fn test_sigmoid_at_inf() {
        assert_eq!(sigmoid(std::f64::INFINITY), 1f64);
        assert_eq!(sigmoid(std::f64::NEG_INFINITY), 0f64);
    }
}
fn main() {
    let sizes = vec![2, 3, 1];
    let mut network_builder = NetworkBuilder::new(sizes);

    network_builder.set_training_data(vec![1.0f64]);

    println!("{:?}", network_builder);
}
