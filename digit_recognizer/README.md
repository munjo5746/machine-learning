# Digit Recognizer

## Cost function
The cost function (MSE, Mean Squared Error) used in this code defined as `C(w,b) = (1/2*n) * sum(||y(x) -
a||^2)`, where `||*||` length of a vector, `y(x)` is the estimation and `a` is
label vector.

### Key facts
  - `C(w,b)` is non-negative
  - `C(w,b)` is approximately equals to 0 if `y(x)` is close to `a`
  - Since the function `C` is a function of `w` and `b`, the goal would be
    finding `w` and `b` whiich makes `C` equals to 0.
  - For a function, the gradient tells use the direction it increase the most.
    The opposite direction of the gradient will tell us the direction the the
    function decreases the most. With a scalar value multiplied by the gradient
    will decrease the function value.
  - To make training more efficient, we can take mini-batch training set by
    picking input randomly. We run this mini-batch training a certain number of
    time. That certain number is called *epoch*.
